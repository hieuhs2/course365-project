/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Khai báo mảng chứa thông tin khóa học
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function() {
    //gọi sụ kiện tải trang
    onPageLoading();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    /*Subtask 1 (40.10) */
    //lấy courses popular
    var vPopularCourses = getPopularCourses(gCoursesDB.courses);
    console.log(vPopularCourses);
    //hiển thị courses popular ra HTML
    //truy vấn thẻ div courses trending
    var vDivPopularElement = $("#popular-courses");
    showCoursesToHtml(vPopularCourses, vDivPopularElement);

    /*Subtask 2 (40.10) */
    //lấy courses trending
    var vTrendingCourses = getTrendingCourses(gCoursesDB.courses);
    console.log(vTrendingCourses);
    //hiển thị courses trending ra HTML
    //truy vấn thẻ div courses trending
    var vDivTrendingElement = $("#trending-courses");
    showCoursesToHtml(vTrendingCourses, vDivTrendingElement);
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm lấy courses popular
function getPopularCourses(paramCourses) {
    var vPopularCourses = paramCourses.filter(courses => courses.isPopular == true);
    return vPopularCourses;
}

//hàm lấy courses trending
function getTrendingCourses(paramCourses) {
    var vTrendingCourses = paramCourses.filter(courses => courses.isTrending == true);
    return vTrendingCourses;
}

//hàm hiển thị courses ra HTML
function showCoursesToHtml(paramTrendingCourses, paramCoursesElement) {
    //xoá trắng nội dung trong div
    paramCoursesElement.html("");

    paramTrendingCourses.map(courses => {
        var vContent = `
            <div class="col-lg-3 col-sm-6 mb-5">
                <div class="card h-100">
                    <img src=${courses.coverImage} class="card-img-top" alt="course angular">
                    <div class="card-body">
                        <h6 class="card-title text-primary">${courses.courseName}</h6>
                        <div class="course-description">
                            <span class="course-duration mr-2"><i class="far fa-clock"></i> ${courses.duration}</span>
                            <span class="course-level">${courses.level}</span>
                        </div>
                        <div class="course-price mt-3">
                            <strong class="course-discount-price">${courses.discountPrice}</strong>
                            <del class="course-initial-price text-secondary">$${courses.price}</del>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row align-items-center">
                            <div class="col-sm-3">
                                <img src=${courses.teacherPhoto} alt="teacher image" class="rounded-circle" width="40">
                            </div>
                            <small class="col-sm-7 teacher-name">${courses.teacherName}</small>
                            <div class="col-sm-2">
                                <i class="far fa-bookmark"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `
        $(vContent).appendTo(paramCoursesElement);
    });
    console.log("Đã hiển thị courses ra HTML");
}